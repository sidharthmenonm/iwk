module.exports = {
	mode: 'jit',

	// you dont need `purge: enabled: production` because you are using jit
	purge: [
		'./src/**/*.svelte',
		// may also want to include HTML files
		'./src/**/*.html'
	],

	darkMode: 'class',

	theme: {
		extend: {
			fontFamily: {
				'josefin': ['Josefin Sans', 'sans-serif', 'ui-sans-serif', 'system-ui', '-apple-system'],
			},
			colors: {
				'iwk-green': '#22B244',
				'iwk-blue': '#00AEEF',
				'iwk-red': '#F9421E',
			},
		}
	},

	variants: {},
	plugins: [],
	content: ['./src/**/*.{html,js,svelte,ts}']
};
