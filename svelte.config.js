import adapter from '@sveltejs/adapter-static';

import preprocess from 'svelte-preprocess';

import dotenv from 'dotenv';
dotenv.config();

/** @type {import('@sveltejs/kit').Config} */
const config = {
	preprocess: [
		preprocess({
			postcss: true,
			scss: {
				prependData: '@import "src/variables.scss";'
			}
		})
	],
	kit: {
		adapter: adapter(),

		// hydrate the <div id="svelte"> element in src/app.html
		// target: '#svelte',
		paths: {
			base: process.env.BASE_URL
		},
		trailingSlash: 'always',
		prerender: {
			default: true,
			enabled: true
		},
		vite: {
			css: {
				preprocessorOptions: {
					scss: {
						additionalData: '@import "src/variables.scss";'
					}
				}
			},
			server: {
				proxy: {
					'/api': {
						target: 'https://iwkerala.org/api',
						changeOrigin: true,
						rewrite: (path) => path.replace(/^\/api/, '')
					},
				}
			}
		}
	}
};

export default config;
